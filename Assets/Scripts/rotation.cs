﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            transform.Rotate(0, 0, 90); //for rotating counter clockwise

        if (Input.GetKeyDown(KeyCode.RightArrow))
            transform.Rotate(0, 0, -90);//for rotating clockwise
    }
}
